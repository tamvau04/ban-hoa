<h2 style="text-align: center; color: red;">Các sản phẩm hoa</h2>
<?php
$total = 0;
if (isset($_SESSION['cart']) && $_SESSION['cart'] != null) {
	foreach ($_SESSION['cart'] as $value) {
		$total += $value['quantity'];
	}
}
?>
<h4 style="text-align: center; margin-top: 10px;"><a href="index.php?page=cart">Bạn đang có <?php echo $total;?> sản phẩm trong giỏ hàng</a></h4>
<?php foreach ($_SESSION['listPage'] as $value) {?>
<div class="produce">
	<a href="index.php?page=product&id=<?php echo $value['id_Product'];?>" class="sp"><img src="template/image/<?php echo $value['images'];?>" width="140px" height = "120px"></a>
	<p class="name"><?php echo $value['name'];?></p>
	<p class="price"><?php echo number_format($value['price']);?> đồng</p>
	<p class="detail"><a href="index.php?page=cart&action=insert&id=<?php echo $value['id_Product'];?>">Mua hàng</a></p>
</div>	
<?php }?>
<div style="padding-left: 400px; padding-top: 20px;">
<?php 
if (isset($_SESSION['totalPage'])) {
	for($i = 1; $i <= $_SESSION['totalPage']; $i++){ ?>
	<p class="page"><a href="index.php?page=product&p=<?php echo $i;?>"><?php echo $i;?></a></p>
<?php }} ?>
</div>


