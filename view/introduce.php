<div style="font-size: 18px; padding: 10px 31px; height: 450px;">
	<h2 style="text-align: center; color: red;">Giới thiệu của hàng</h2>
	<p style="margin-top: 20px;">Mellisa Flower trân trọng gửi đến khách hàng lời chúc sức khỏe và lời chào nồng nhiệt nhất.</p>
	<p>Với phương trâm mang đến sự tiện lợi của khách hàng là hạnh phúc của chúng tôi. Meliisa Flower ra đời nhằm thực hiện sứ mệnh đó.</p>
	<p style="float: left; margin-right: 20px; margin-top: 10px; margin-bottom: 10px;"><img src="template/image/hb12.png" width="170px" height="150px"></p>
	<p style="margin: 10px;">Mellisa Flower tự hào là website uy tín về các lĩnh vực hoa tươi, hoa nghệ thuật, hoa sinh nhật, hoa chúc mừng, hoa bó, hoa văn phòng...</p>
	<p>Đến với Mellisa Flower là đến với thế giới hoa tươi với những mẫu hoa mới nhất, đẹp nhất của các shop hoa tươi nghệ thuật trên toàn quốc với giá cả và dịch vụ cạnh tranh nhất.</p>
	<p style="margin: 10px;">Để đáp ưng nhu cầu đa dạng và ngày càng cao của khách hàng, Mellisa Flower cung cấp các dịch vụ hoa đến tận các địa chỉ theo đơn đặt hàng của khách hàng.</p>
	<p style="margin: 10px;">Với phương trâm "Vui lòng người nhận, đẹp lòng người trao". Quý khách sẽ yên tâm và hài lòng mỗi khi sử dụng dịch vụ của Mellisa Flower.</p>
	<p style="margin: 10px;">Hãy truy cập vào website hoặc gọi đến số điện thoại 0164 9396 961 để được tư vấn miễn phí.</p>
</div>


