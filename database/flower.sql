-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2016 at 05:29 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flower`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
`id_Article` int(11) NOT NULL,
  `title_article` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_article` text COLLATE utf8_unicode_ci NOT NULL,
  `images_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `images_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail_1` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_2` text COLLATE utf8_unicode_ci NOT NULL,
  `id_News` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng chi tiết các bài tin tức';

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id_Article`, `title_article`, `description_article`, `images_1`, `images_2`, `detail_1`, `detail_2`, `id_News`) VALUES
(1, 'Hoa hồng - Tình yêu', 'Lễ cưới là một ngày trọng đại của đôi uyên ương. Để chuẩn bị cho một đám cưới hoàn hảo cũng là điều không dễ dàng.', 'hoahong.jpg', '', 'Hoa hồng là hoa thể hiện sự sôi nổi, cháy bỏng trong tình yêu, ngoài vẻ đẹp quyến rũ và thanh lịch thì hoa hồng cũng là hoa cưới được dùng phổ biến nhất. </br></br>\r\nChính sự phong phú về màu sắc mà hoa hồng được rất nhiều cô dâu lựa chọn làm màu sắc chủ đạo trong tiệc cưới của mình. Ví dụ, hoa hồng đỏ đại diện cho tình yêu và sự lãng mạn, hoa hồng trắng thể hiện sự tinh khiết và sang trọng, hoa hồng màu vàng hoặc cam chứng tỏ bạn là một người cá tính và luôn thích sự sôi động, vui vẻ... </br></br>\r\nNhiều cô dâu sau khi lựa chọn màu sắc chủ đạo cho đám cưới thì sẽ chọn những hoa cưới phù hợp ý nghĩa đằng sau từng loại hoa.\r\n', '', 1),
(2, 'Tulip - Loài hoa mạnh mẽ', 'Hoa tulip thường được kết hợp với đám cưới mùa xuân, nó tượng trưng cho tượng trưng cho sự thanh khiết, trinh nguyên, ngọt ngào và chân thành.', 'tulip.jpg', '', 'Hoa tulip thường được kết hợp với đám cưới mùa xuân, nó tượng trưng cho tượng trưng cho sự thanh khiết, trinh nguyên, ngọt ngào và chân thành.</br></br>\r\nNhiều người không biết rằng hoa tulip cũng có một lịch sử lâu dài đại diện cho tình yêu vĩnh cửu. Cũng giống với hoa hồng, từng màu sắc khác nhau mà hoa tulip có ý nghĩa khác nhau. Chính sự đa dạng đó mà bạn có thể dễ dàng kết hợp hoa tulip với các chủ đề đám cưới khác nhau, hoặc để làm nổi bật thêm bó hoa cươícủa cô dâu.</br></br>\r\nLan hồ điệp (Phaleanopsis) - Tình yêu thật sự Lan hồ điệp hay còn gọi là Hoa bướm với những hình dáng những cánh hoa đúng như tên gọi. Tuy không được sử dụng phổ biến làm hoa cưới nhưng lại mang đậm chất thời thượng, tạo điểm nhấn đặc biệt trong buổi lễ.', '', 1),
(3, 'Tưng bừng Lễ hội Hoa Ban 2015 của 19 dân tộc tỉnh Điện Biên', 'Tối 13/3, tại Quảng trường 7/5 thành phố Điện Biên Phủ, tỉnh Điện Biên, đã long trọng khai mạc Lễ hội Hoa Ban năm 2015. \r\n', 'lehoihoaban.jpg', 'lehoihoaban1.jpg', 'Đây là lễ hội thường niên được địa phương tổ chức để chào mừng kỷ niệm các ngày lễ lớn của đất nước và của tỉnh và cũng là lễ hội góp phần tôn vinh, bảo tồn, phát huy các loại hình văn hóa dân tộc; giới thiệu hình ảnh Điện Biên với bạn bè trong nước và quốc tế, phát huy tiềm năng du lịch, phát triển kinh tế-xã hội của địa phương nơi cực Tây của Tổ quốc. </br></br>\r\n\r\nTham dự lễ khai mạc có đại diện lãnh đạo thường trực Tỉnh ủy, Hội đồng Nhân dân, Ủy ban Nhân dân tỉnh Điện Biên; Đoàn công tác Ủy ban các vấn đề xã hội Quốc hội Việt Nam; đại biểu các tỉnh Tây Bắc mở rộng; Đại sứ quán Malaysia tại Việt Nam… cùng hàng ngàn diễn viên, vận động viên đến từ 10/10 huyện, thị, thành phố, các cơ quan, đơn vị lực lượng vũ trang, học sinh-sinh viên các trường chuyên nghiệp, các bản văn hóa và nhân dân các dân tộc trên địa bàn tỉnh. </br></br>\r\n', 'Phát biểu tại lễ khai mạc, ông Mùa A Sơn, Chủ tịch Ủy ban Nhân dân tỉnh nhấn mạnh Điện Biên tự hào là vùng đất của hoa ban, nơi sinh sống của 9 dân tộc anh em. Đúng vào ngày cách đây 61 năm, tiếng súng mở màn Chiến dịch Điện Biên Phủ vang lên, bắt đầu chiến dịch 56 ngày đêm giải phóng miền Tây Bắc. Ngày hôm nay, Điện Biên long trọng tổ chức Lễ hội Hoa Ban là hành động thiết thực góp phần bảo tồn, tôn vinh và phát huy các loại hình di sản văn hóa dân tộc tỉnh Điện Biên, gắn sản phẩm văn hóa với phát triển tiềm năng du lịch, phát triển kinh tế xã hội; giới thiệu với du khách trong và ngoài nước vẻ đẹp của mảnh đất và con người Điện Biên. </br></br>\r\n\r\nLễ hội Hoa Ban được tổ chức thường niên vào dịp trung tuần tháng Ba hàng năm, là sản phẩm văn hóa du lịch đặc trưng của Điện Biên nói riêng và Tây Bắc nói chung. </br></br>\r\n\r\nĐiện Biên xác định việc tổ chức, xây dựng thương hiệu Lễ hội Hoa Ban sẽ góp phần thực hiện thắng lợi chiến lược phát triển văn hóa, du lịch của tỉnh... </br></br>\r\n\r\nLễ hội Hoa Ban năm nay được mở màn bằng chương trình nghệ thuật ''''Về miền Hoa Ban'''' với sự tham gia biểu diễn của hơn 200 diễn viên, đến từ Đoàn nghệ thuật tỉnh Điện Biên, Trung tâm Văn hóa tỉnh, Đoàn ca múa nhạc Nhà hát Tuổi trẻ và một số đơn vị văn hóa, lực lượng vũ trang, các trường chuyên nghiệp trên địa bàn tỉnh. </br></br>\r\n\r\nVới 3 chương ''''Cội nguồn Hoa Ban'''', ''''Lời mời Điện Biên'''' và ''''Mường Thanh tụ hội,'''' chương trình nghệ thuật đặc sắc này đã đem đến cho khán giả ấn tượng mạnh mẽ về huyền thoại hoa ban của dân tộc Thái từ mối tình chung thủy, trong trắng của chàng Khum và nàng Ban. <br></br>\r\n\r\nMỗi điệu múa, lời ca, hoa văn trang phục… của các diễn viên đều góp phần thể hiện những nét văn hóa đặc trưng trong đời sống sinh hoạt, sản xuất của đồng bào 19 dân tộc anh em sinh sống ở Điện Biên. <br></br>\r\n\r\nLễ hội Hoa Ban 2015 sẽ diễn ra đến ngày 15/3 với các cuộc thi dân ca, dân vũ, âm nhạc truyền thống tại Trung tâm hội nghị văn hóa tỉnh. Theo đó, 10 đoàn đến từ các địa phương trong tỉnh sẽ tham gia thi đấu các môn thể thao và trò chơi truyền thống dân tộc; trình diễn trang phục truyền thống dân tộc; trình diễn các lễ hội truyền thống; thi ẩm thực ''''Hương sắc Điện Biên.''</br></br>\r\n\r\nCũng tại lễ hội này, các đơn vị, địa phương và doanh nghiệp sẽ tham gia trưng bày sản phẩm quà lưu niệm, thủ công truyền thống, các sản phẩm mang đậm hương vị đặc trưng của núi rừng Điện Biên; trưng bày các sản phẩm văn hóa về đời sống sinh hoạt, tín ngưỡng, lễ hội truyền thống của đồng bào các dân tộc trong tỉnh; giới thiệu về hoa ban, các hình ảnh đẹp, các tài liệu hướng dẫn trồng và chăm sóc hoa ban, cung cấp giống cây hoa ban cho du khách có nhu cầu…</br></br>\r\n\r\nLễ hội Hoa Ban được tỉnh Điện Biên tổ chức thường niên vào mỗi dịp hoa ban nở, gắn liền với kỷ niệm ngày 13/3 là ngày mở màn Chiến dịch Điện Biên Phủ.</br></br>\r\n\r\nThông qua lễ hội này, hình tượng hoa ban nói riêng, vẻ đẹp của mảnh đất, con người Điện Biên nói chung đã được giới thiệu đến đông đảo bạn bè trong nước và quốc tế. Hoa ban là hình ảnh xuyên suốt trong lễ hội, đây là loài hoa có trong truyền thuyết và hiện hữu trong cuộc sống hàng ngày của người dân, gắn liền với những giá trị lịch sử, văn hóa truyền thống của cộng đồng các dân tộc địa phương. Qua đó thể hiện mối quan hệ hài hòa giữa con người với môi trường thiên nhiên và văn hóa. </br></br>\r\n\r\nĐây là lần thứ 2, Lễ hội Hoa Ban Điện Biên được tổ chức. Lần đầu tiên, Lễ hội diễn ra cùng các sự kiện văn hóa chào mừng Kỷ niệm 60 năm Chiến thắng Điện Biên Phủ (7/5/1954-7/5/2014).', 2),
(4, 'Chiêm ngưỡng vẻ đẹp nơi thủ phủ tam giác mạch', 'Một mùa hoa tam giác mạch Hà Giang lại đến. Năm nay, lễ hội Hoa tam giác mạch lần đầu tiên sẽ được tổ chức ở cao nguyên đá Đồng Văn. ', 'tamgiacmach.jpg', 'tamgiacmach1.jpg', 'Một mùa hoa tam giác mạch Hà Giang lại đến. Năm nay, lễ hội Hoa tam giác mạch lần đầu tiên sẽ được tổ chức ở cao nguyên đá Đồng Văn. Hãy cùng chiêm ngưỡng vẻ đẹp của loài hoa này cùng với cuộc sống của người dân vùng cao nguyên đá trước ngày lễ hội…</br></br>\r\n\r\nLễ hội Hoa tam giác mạch Hà Giang năm nay diễn ra từ ngày 12 đến ngày 15 tháng 11 năm 2015 tại Cao nguyên đá Đồng Văn, tỉnh Hà Giang. Theo đó, lễ hội với chủ đề “Cao nguyên đá – Ngàn hoa khoe sắc” sẽ chính thức khai mạc vào tối 13/11/2015 tại Thị trấn Đồng Văn.</br></br>', 'Hoa tam giác mạch còn có tên gọi khác là kiều mạch. Loài hoa này thường mọc thành từng chùm. Mùa hoa tam giác mạch diễn ra vào khoảng tháng 10 – 11 hàng năm. Màu của hoa tam giác mạch biến đổi theo vòng đời của hoa: khi mới nở hoa có màu trắng, sau đó chuyển sang phớt hồng, rồi ánh tím, và cuối cùng là đỏ sẫm.</br></br>\r\n\r\nHà Giang hiện đang là nơi có nhiều hoa tam giác mạch nhất ở Việt Nam. Nơi đây không chỉ hấp dẫn dân du lịch, đặc biệt là các phượt thủ tới khám phá phong cảnh núi non hùng vĩ của Cao nguyên đá Đồng Văn mà cả những cánh đồng hoa tam giác mạch trải dài mênh mông một vùng núi.</br></br>\r\n\r\nNgoài Cao nguyên đá Đồng Văn, cánh đồng hoa tam giác mạch đã trở thành biểu tượng, sản phẩm du lịch độc đáo riêng có ở Hà Giang.</br></br>\r\n\r\nNhưng bạn chỉ có thể thụ cảm được vẻ đẹp hoa tam giác mạch trong một không gian bao la, phóng khoáng. Những triền đồi rực một màu tím hồng đem lại một niềm say đắm, khoái lạc không bút nào tả xiết.\r\n', 2),
(5, 'Hoa anh túc – hoa của sự lãng quên, ảo tưởng', 'Hoa Anh Túc là danh xưng chung một tộc hoa “hữu sắc vô hương”, có tên khoa học là Papaver, tiếng pháp là pavot và tiếng anh là poppy.', 'hoaanhtuc.jpg', 'hoaanhtuc1.jpg', 'Hoa Anh Túc là danh xưng chung một tộc hoa “hữu sắc vô hương”, có tên khoa học là Papaver, tiếng pháp là pavot và tiếng anh là poppy.</br></br>\r\nNgười mình gọi nôm na Anh Túc là hoa á phiện. Văn nhơn thi sĩ thi vị hóa, gọi á phiện là Phù Dung. Và có lẽ để tránh nhìn lầm nó với một loại phù dung khác (bông bụp hay hoa dâm bụt – bụt mà còn dâm trời ạ) họ gọi Phù Dung á phiện là nàng, là tiên tử. Gia tộc Anh Túc Papaver có rất nhiều loại, từ loại dùng làm kiểng cho tới loại dùng làm thuốc.</br></br>', 'Tháng 5/1915, chuyến chuyển quân ngang đồng có xứ Flanders (Flandres, nay thuộc Belgium) bạt ngàn anh túc đỏ khiến ông liên tưởng đến các chiền sĩ gục ngã trong trận giao tranh ác liệt xảy ra tại vùng đất khô cằn này. Truyền thuyết của cư dân tại đó kể rằng, các bông anh túc Papaver rhoas đỏ thắm tươi tốt là vì chúng đã thấm máu các chiến sĩ vô danh hy sinh trên chiến địa.</br></br>\r\n\r\nAnh Túc có rất nhiều loại, từ loại dùng làm kiểng cho đến loại làm thuốc. Hoa Mào gà cũng thuộc gia tộc của hoa Anh túc, hoa mào gà được dùng làm kiểng, được trưng bày khắp chợ trong mỗi dịp tết.</br></br>\r\n\r\nHoa Anh túc có ba màu chủ yếu: màu đỏ, màu trắng và màu vàng. ngoài ra nó còn có màu tí và màu cam, nhưng rất ít gặp. Mỗi màu với những ý nghĩa khác nhau.</br></br>\r\n\r\nHoa Anh túc trắng tượng trưng cho sự an ủi. Hoa Anh túc vàng là biểu tượng của sự giàu có, uy lực và thành công. Hoa Anh túc đỏ là sự khoái lạc, sự quyến rũ phù du. Nhưng nổi lên trên hết, 3 màu hoa đều có ý nghĩa chung là sự ảo tưởng, sự lãng quên, giấc ngủ thiên thu và một sự mê hoặc.', 3),
(6, 'Sự tích hoa mộc lan', 'Hoa mộc lan còn có tên là bạch ngọc lan, ngọc lan hoa. Cây có thân gỗ lâu năm được trồng rộng rãi ở ven đường của nhiều nước bởi vẻ đẹp tinh khiết của hoa cùng hương thơm nhẹ nhàng rất hấp dẫn. ', 'hoamoclan.jpg', 'hoamoclan1.jpg', 'Hoa mộc lan còn có tên là bạch ngọc lan, ngọc lan hoa. Cây có thân gỗ lâu năm được trồng rộng rãi ở ven đường của nhiều nước bởi vẻ đẹp tinh khiết của hoa cùng hương thơm nhẹ nhàng rất hấp dẫn. Hình dạng hoa mộc lan của chúng sắp xếp thành vòng, các loài trong họ có nhị và nhụy hoa sắp xếp thành hình xoắn ốc trên đế hoa hình nó. Hoa mộc lan có nguồn gốc từ Châu Á vì vậy mà sự tích hoa mộc lan đã được viết lên tại nước Nhật xinh đẹp.\r\n</br></br>\r\nTruyện kể rằng:\r\n</br></br>\r\nTrên đất Nhật, có nàng Keiko mồ côi cha mẹ từ nhỏ và phải tự kiếm sống bằng nghề làm hoa giấy. Nàng làm việc vất vả suốt từ sáng sớm đến tối mịt, nhưng hoa bán được nhiều mà lãi chẳng đáng là bao. Làm việc mệt nhọc mà vẫn không có nhiều tiền để mua cho mình một bộ kimono, nàng lấy làm buồn lòng.</br></br>\r\nRồi đến một ngày, có một chú vẹt đã cho nàng biết được bí quyết làm hoa giấy thành hoa thật nhờ vào việc tiếp tế cho chúng những giọt máu của chính mình. Khi biết được nàng đã nhỏ những giọt máu lên hoa giả để biến chúng thành hoa tươi.</br></br>', 'Hoa của Keiko trở nên đẹp một cách kỳ lạ và nàng làm không kịp bán. Keiko giàu lên nhanh chóng. Tiền đã giúp cô thay đổi thành một thiếu nữ xinh đẹp, điệu đà. Tiền đã đưa cô đến với vũ hội, gặp được người cô yêu. Người yêu của Keiko muốn rằng cô sẽ kiếm đủ tiền để mua cho cả hai một ngôi biệt thự.</br></br>\r\n\r\nĐể thực hiện ước mơ này, hoa tươi cần được bán nhiều hơn, Keiko bắt đầu lao động cật lực hơn và tất nhiên máu ở đầu ngón tay cô cũng phải chích nhiều hơn. Rồi Keiko cũng mua được ngôi nhà nhưng rất nhỏ nên chẳng làm chồng nàng thỏa nguyện. Hoa tươi lại buộc phải bán nhiều hơn nữa. Nhiều mãi, nhiều mãi cho đến một ngày cuối cùng của nàng cũng bị vắt kiệt trước cái giá quá hời của một vị khách Pháp.</br></br>\r\nCây hoa với một bông đỏ thắm này đã biến giấc mơ sống trong biệt thự của người chồng ích kỷ thành sự thực, nhưng nó cũng lấy đi hơi thở cuối cùng của Keiko. Còn vị khách đặc biệt yêu thích hoa tươi của Keiko thì hân hoan mang nó về nước và trìu mến gọi nó là mộc lan. Từ đó, loài người cùng lúc có thêm một huyền thoại buồn và một loài hoa quý mang tên Hoa Mộc Lan.</br></br>', 3),
(8, 'Ý nghĩa hoa đồng tiền', 'Đồng tiền hay cúc đồng tiền (Hoa kim tiền) là loài hoa thuộc họ hoa cúc với tên tiếng Anh là Gerber được đặt theo tên nhà tự nhiên học người Đức Traugott Gerber.', 'dongtien.jpg', 'dongtien1.jpg', 'Đồng tiền hay cúc đồng tiền (Hoa kim tiền) là loài hoa thuộc họ hoa cúc với tên tiếng Anh là Gerber được đặt theo tên nhà tự nhiên học người Đức Traugott Gerber. Hoa Đồng tiền được trồng làm cảnh với nhiều màu sắc khác nhau như: Đỏ, cam, hồng, vàng, tím .... ngoài ra hoa đồng tiền cũng được các nghệ nhân ưa chuộng sử dụng trong các mẫu cắm hoa. có thể cắm chúng với các loài hoa khác hoặc cắm 1 mình cũng được.\r\n<br><br>\r\nHoa đồng tiền, loài hoa thể hiện sự hạnh phúc, mang đến sự tươi sáng và vui vẻ, đồng thời thể hiện sự ngay thơ và lòng ca ngợi.\r\n<br><br>\r\nCó rất nhiều loài hoa có thể bày tỏ suy nghĩ và tình cảm của bạn, nhưng hoa đồng tiền luôn nổi bật hẳn lên bởi sự rực rỡ đầy tươi vui của chúng. Khi bạn tặng cho ai đó những bông hoa này, chúng không những truyền đạt cho họ những thông điệp sâu sắc đầy ý nghĩa mà còn giúp đem lại một ấn tượng khó quên.<br><br>', 'Như nói ở trên Hoa đồng tiền có rất nhiều màu sắc và mỗi màu lại mang 1 ý nghĩa khác nhau.<br><br>\r\nHoa đồng tiền trắng: thể hiện sự trong trắng tinh khiết<br>\r\nHoa đồng tiền vàng: thể hiện niềm hạnh phúc<br>\r\nHoa đồng tiền hồng: thể hiện sự ca ngợi khích lệ<br>\r\nHoa đồng tiền đỏ: thể hiện tình yêu thắm đợm<br><br>\r\nNhưng dù là màu gì đi nữa thì loài hoa này đều tượng trưng cho sự hạnh phúc. Nó còn mang ý nghĩa về vẻ đẹp và điều kỳ diện. Nó mang đến cho chúng ta sự tươi sáng và vui vẻ. Không những thế, nó còn thể hiện sự ngây thơ, tình yêu và lòng ca ngợi.<br><br>\r\n\r\nHoa đồng tiền tượng trưng cho sự tin tưởng và sự sôi nổi. Nếu bạn muốn chinh phục trái tim của một cô gái cá tính, tự tin và sôi nổi hãy tặng cô ấy một chậu hoa đồng tiền. Những bông hoa màu hồng dễ thương vươn thẳng lên từ tán lá xanh thẫm thể hiện một cá tính độc lập nhưng vẫn dịu dàng, mềm mại. Cánh hoa xoè rộng, màu sắc tươi sáng thể hiện sức sống mãnh liệt, sự nhiệt tình đến tuyệt vời. \r\n<br><br>\r\nNgoài ra hoa đồng tiền còn gọi là cây kim tiền. Trong những dịp đầu xuân năm mới, nếu trồng hoa hoặc cắm hoa sẽ mang đến nhiều may mắn, tài lộc và tiền của cho gia đình\r\n<br><br>', 3),
(9, 'Những điều cần biết về festival hoa Đà Lạt', 'Mặc dù còn cách ngày khai mạc khá xa nhưng Festival Hoa Đà Lạt 2015 đã thu hút sự chú ý và quan tâm của rất nhiều du khách trong nước và quốc tế. ', 'festival1.jpg', 'festival2.jpg', 'Theo dự kiến, Festival Hoa Đà Lạt lần thứ VI – năm 2015 với chủ đề “Đà Lạt – muôn màu sắc hoa”. sẽ diễn ra trong 5 ngày, từ ngày 29/12/2015 đến hết ngày 02/01/2016 tại TP. Đà Lạt và một số địa phương trong tỉnh.\r\nLà một trong những lễ hội lớn nhất của tỉnh Lâm Đồng thu hút rất nhiều du khách trong và ngoài nước tham gia khám phá, Festival Hoa Đà Lạt luôn được đầu tư rất công phu và kỹ lưỡng.\r\n', 'Theo đó, Festival Hoa Đà Lạt lần thứ VI – năm 2015 dự kiến sẽ gồm có 8 chương trình chính vô cùng đặc sắc, đó là:\r\n<br><br>\r\nKhông gian hoa: Không gian hoa được tổ chức xung quanh hồ Xuân Hương từ ngày 30/12/2015 – 03/01/2016 với rất nhiều tiểu cảnh về hoa cùng những cuộc thi với giải thưởng cao. Những tiểu cảnh này được duy trì trong suốt thời gian diễn ra lễ hội để phục vụ hoạt động tham quan, mua sắm của du khách.<br<br>\r\nTrưng bày, triển lãm hoa, cây cảnh Đà Lạt 2015: Hoạt động này diễn ra tại vườn hoa thành phố Đà Lạt, kéo dài từ ngày 30/12/2015 – 03/01/2016. Hoạt động này không chỉ thu hút đông đảo các tổ chức, cá nhân có thế mạnh về sản xuất, kinh doanh hoa ở trong và ngoài tỉnh mà còn có sự góp mặt của nhiều doanh nghiệp quốc tế, thu hút đông đảo du khách tham dự.<br><br>\r\nLễ khai mạc Festival Hoa Đà Lạt lần thứ VI – Năm 2015: Phần lễ khai mạc Festival Hoa năm nay sẽ được tổ chức vào lúc 20 giờ, ngày 30/12/2015 tại quảng trường Lâm Viên. Lễ khai mạc ấn tượng này bao gồm: Chương trình khai mạc, các chương trình nghệ thuật đặc sắc và thực hiện diễu hành Carnaval Hoa Đà Lạt.<br><br>\r\nCarnaval Hoa Đà Lạt: Là hoạt động sôi nổi diễn ra từ ngày 30/12/2015 – 03/01/2016. Đoàn diễu hành gồm nhiều xe hoa; các nhóm biểu diễn nghệ thuật đường phố; nhóm hóa trang ấn tượng,… Một số nhóm tham gia Carnaval biểu diễn những tiết mục được chọn lọc tại khu vực quảng trường Lâm Viên và xung quanh hồ Xuân Hương.<br><br>\r\nPhiên chợ rau, hoa Đà Lạt: Phiên chợ này được tổ chức trên đường Nguyễn Văn Cừ nối dài và đường Nguyễn Thị Minh Khai, diễn ra từ ngày 30/12/2015 – 03/01/2016. Bao gồm không gian trưng bày nghệ thuật hoa, rau; nơi giới thiệu và ký kết hợp đồng thương mại về các loại rau, hoa Đà Lạt, khu vực giới thiệu ẩm thực chế biến từ các loại rau, củ, quả Đà Lạt.', 2);

-- --------------------------------------------------------

--
-- Table structure for table `authority`
--

CREATE TABLE IF NOT EXISTS `authority` (
`id_authority` int(50) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `authority`
--

INSERT INTO `authority` (`id_authority`, `name`) VALUES
(1, 'Quản trị'),
(2, 'Khách hàng'),
(3, 'Quản lý');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id_News` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `images` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Bảng tin tức';

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id_News`, `title`, `images`, `description`) VALUES
(1, 'Hoa ngày cưới', 'img5.jpg', 'Lễ cưới là một ngày trọng đại của đôi uyên ương. Để chuẩn bị cho một đám cưới hoàn hảo cũng là điều không dễ dàng.'),
(2, 'Lễ hội hoa', 'img6.jpg', 'Sự kiện về hoa, thông tin về những loại hoa, sự kiện, cuộc thi, tin tức về hoa cho những người yêu và quan tâm đến hoa. Như festival hoa Đà Lạt'),
(3, 'Ý nghĩa hoa', 'img7.jpg', 'Mỗi loài hoa có một ý nghĩa tặng khác nhau mà bạn nên biết, điều đó giúp bó hoa của bạn truyền tải thông tin tốt hơn tới người được tặng hoa');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id_Product` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `images` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng các sản phẩm hoa của cửa hàng';

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_Product`, `name`, `images`, `price`, `description`) VALUES
(1, 'Hoa bó HB1', 'img1.jpg', 200000, 'Bó hoa với 10 hồng da, cùng với Vũ Nữ , calimero sang trọng và quý phái.'),
(2, 'Hoa bó HB2', 'img2.jpg', 300000, 'Bó hoa như một bản tình ca du dương, là sự kết hợp giữa các loại hoa tự nhiên và quý hiếm: hoa arum, hồng tím, mõm sói, lục môn, thủy tiên, caramel.'),
(3, 'Hoa bó HB3', 'img3.jpg', 100000, 'Bó hoa gồm 10 hoa hồng màu hồng , cẩm tú cầu và mokara tím tạo vẻ đẹp rất Tây và sang trọng.'),
(4, 'Hoa bó HB4', 'img4.jpg', 500000, 'Bó hoa là sự phối hợp giữa các sắc màu của lan Hồ Điệp quý phái.'),
(5, 'Hoa bó HB5', 'hb5.jpg', 450000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm<br>\r\nBao gồm:<br><br>\r\n\r\nHoa hồng - 10 cành<br>\r\nCẩm chướng nhánh - 20 cành<br>\r\nKỳ lân - 10 cành<br>\r\nLá chanh - 10 cành<br>'),
(6, 'Mùa xuân', 'hb6.png', 450000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nHoa hồng - 10 cành<br>\r\nCẩm chướng nhánh - 20 cành<br>\r\nKỳ lân - 10 cành<br>\r\nLá chanh - 10 cành<br>\r\nGiỏ bố'),
(7, 'Lan hồ điệp', 'hb7.png', 600000, 'Lan hồ điệp 2 vòi - 2 chậu<br>\r\nNguyệt quế - 2 chậu<br>\r\nChậu sứ <br>\r\nPhụ liệu<br><br>\r\n\r\nGhi chú: Lan hồ điệp size mini là loại lan được sản xuất độc quyền tại Dalat Hasfarm. Loại lan này trang trí rất dễ thương khi để trên bàn làm việc hoặc bàn uống nước. Và không phải loại hoa lan cỡ lớn thường bày bán tại thị trường.'),
(8, 'Mùa hè rực rỡ', 'hb11.png', 400000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm<br><br>\r\n\r\nBao gồm:<br><br>\r\n\r\nChậu thu hải đường - 1 chậu<br>\r\nSống đời trung kép - 1 chậu<br>\r\nSống đời mini kép - 2 chậu<br>\r\npico mini - 2 chậu<br>\r\nHộp gỗ vuông 20cm<br><br>\r\n\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm'),
(9, 'Ly chậu màu cam', 'hb9.png', 300000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nChậu lily - 2 chậu<br><br>\r\nGiỏ mây\r\n<br><br>\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm'),
(10, 'Lan tím yêu thương', 'hb10.png', 350000, 'Lan hồ điệp size mini là loại lan được sản xuất độc quyền tại Dalat Hasfarm. Loại lan này trang trí rất dễ thương khi để trên bàn làm việc hoặc bàn uống nước.<br><br>\r\n\r\nBao gồm:<br><br>\r\n\r\nLan hồ điệp 2 vòi - 1 chậu<br>\r\nSống đời mini - 1 chậu<br>\r\nNguyệt quế - 1 chậu<br>\r\n<br><br>\r\nChậu sứ men trắng'),
(11, 'Nhip cầu trái tim', 'hb12.png', 500000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nHồng môn 2 thân - 3 chậu<br><br>\r\nGiỏ mây tròn\r\n<br><br>\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm'),
(12, 'Sao lấp lánh', 'hb13.png', 400000, 'Sản phẩm ly chậu đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nChậu lily - 3 chậu<br>\r\nChậu sứ<br><br>\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm'),
(13, 'Mãi mãi xinh tươi', 'hb14.png', 450000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nSống đời mini kép - 7 chậu<br><br>\r\nGiỏ bố/ Thúng vuông'),
(14, 'Khu vườn xinh xắn', 'hb15.png', 450000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nChậu lily thường màu - 1 chậu<br>\r\nChậu cúc pico trung - 1 chậu<br>\r\nChậu cúc pico mini - 2 chậu<br>\r\nNguyệt quế - 2 chậu<br>\r\nHộp gỗ chữ nhật 15x25cm<br><br>\r\n\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm'),
(15, 'Nụ cười đáng yêu', 'hb16.png', 450000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nCúc pico trung - 4 chậu<br>\r\nCúc pico mini - 2 chậu<br>\r\nNguyệt quế - 2 chậu<br>\r\nGiỏ mây <br><br>\r\n\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm'),
(16, 'Cảm hứng mãi xanh', 'hb17.png', 550000, 'Những cành lan hồ điệp xanh mơn mở đựng trong chiếc giỏ mây mộc mạc gợi nên cảm giác tươi mới, thanh bình và là món quà sinh nhật đầy ý nghĩa cho người thân yêu của bạn.'),
(17, 'Đồi mơ mộng', 'hb18.png', 1000000, 'Bộ sưu tập hoa đặc biệt của Dalat Hasfarm\r\n<br><br>\r\nBao gồm:\r\n<br><br>\r\nLan hồ điệp mini 2 vòi - 4 chậu<br>\r\nNguyệt quế - 2 chậu<br>\r\nSống đời mini đơn -4 chậu<br>\r\nChậu sứ tròn<br><br>\r\n\r\nGhi chú: Quý khách vui lòng đặt trước 2 ngày đối với mẫu này để đảm bảo mẫu mã và số lượng hoa như ý. Hoặc gọi cho chúng tôi theo số: 08.38.333.000 nếu cần tư vấn thêm');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_User` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `id_authority` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_User`, `username`, `password`, `fullname`, `birthday`, `sex`, `id_authority`) VALUES
(1, 'admin', '123456', 'Chử Kim Thắng', '1994-08-14', 1, 1),
(4, 'hoahlu', '123456', 'Trịnh Thị Hoa', '1995-08-21', 0, 2),
(18, 'quangage', '123456', 'Nguyễn Mạnh Quang', '1994-02-17', 1, 2),
(47, 'luongngogia', '123456', 'Ngô Gia Lượng', '1994-02-21', 1, 2),
(16, 'loichu', '123456', 'Chử Kim Lợi', '1996-05-13', 1, 3),
(48, 'canh94', '123456', 'Nguyễn Văn Cảnh', '1994-02-13', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
 ADD PRIMARY KEY (`id_Article`);

--
-- Indexes for table `authority`
--
ALTER TABLE `authority`
 ADD PRIMARY KEY (`id_authority`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id_News`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id_Product`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_User`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
MODIFY `id_Article` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `authority`
--
ALTER TABLE `authority`
MODIFY `id_authority` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id_News` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id_Product` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id_User` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
