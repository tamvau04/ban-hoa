<?php
include_once("model/news.php");
$news = new News();
$_SESSION['list_News'] = $news->getList();

$action = isset($_GET['action']) && $_GET['action'] != null ? $_GET['action'] : '';
switch ($action) {
	case 'insert':
		$_SESSION['category'] = $news->getListCategory();
		include_once("view/news/form.php");
		include_once("control/newsProcess.php");
		break;
	case 'update':
		$id = isset($_GET['id']) && $_GET['id'] != null ? $_GET['id'] :'';
		$_SESSION['category'] = $news->getListCategory();
		$_SESSION['news'] = $news->getOne($id);
		include_once("view/news/form.php");
		include_once("control/newsProcess.php");
		break;
	case 'delete':
		$id = isset($_GET['id']) && $_GET['id'] != null ? $_GET['id'] :'';
		$news->delete($id);
		header("location:index.php?page=news");
		break;
	default:
		if (!isset($_GET['p'])) {
			$pages = 1;
		}else{
			$pages = $_GET['p'];
		}
		$limit = 6;
		$totalPage = ceil(count($_SESSION['list_News'])/$limit);
		$start = ($pages - 1) * $limit;
		$listNews = $news->getListPage($start,$limit);
		// $_SESSION['list_Page_News'] = $news->getListPage($start,$limit);
		include_once("view/news/list.php");
		break;
}
?>