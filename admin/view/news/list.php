<div>
	<h2 style="text-align: center; color: red; margin-bottom: 20px;">Danh sách tin tức</h2>
	<h3 style="text-align: center;">Thêm bài viết</h3>
	<a href="index.php?page=news&action=insert" style="margin-left: 420px;"><img src="../template/image/insert.png" width="70px" height="60px" style="border-radius:10px;"></a>
	<table border="1" width="92%">
		<tr style="background:#7f827b; height: 50px; text-align: center; font-weight: bold;">
			<td width="20%">Tên bài viết</td>
			<td>Mô tả</td>
			<td>Ảnh mô tả</td>
			<td>Chuyên mục</td>
			<td>Sửa</td>
			<td>Xóa</td>
		</tr>
		<?php foreach ($listNews as $value) { ?>
		<tr>
			<td style="font-weight: bold; text-align: center;"><?php echo $value['title_article'];?></td>
			<td style="padding-left: 5px; padding-right: 5px;"><?php echo $value['description_article'];?></td>
			<td><img src="../template/image/<?php echo $value['images_1'];?>" width="100px" height="80px" style="border: 1px solid #7f827b; border-radius:3px;"></td>
			<td style="font-weight: bold; text-align: center;" width="10%"><?php echo $value['title'];?></td>
			<td style="padding-left:5px;"><a href="index.php?page=news&action=update&id=<?php echo $value['id_Article'];?>"><img src="../template/image/edit.jpg" width="50px" height="40px" style="border-radius:10px;"></a></td>
			<td><a href="index.php?page=news&action=delete&id=<?php echo $value['id_Article'];?>"><img src="../template/image/delete.png" width="60px" height="50px"></a></td>
		</tr>
		<?php } ?>
	</table>
	<div style="padding-left: 400px; padding-top: 30px;">
		<?php for($i = 1; $i <= $totalPage; $i++){ ?>
			<p class="page"><a href="index.php?page=news&p=<?php echo $i;?>"><?php echo $i;?></a></p>
		<?php }?>
	</div>
</div>