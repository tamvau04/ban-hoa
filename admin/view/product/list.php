<div>
	<h2 style="text-align: center; color: red; margin-bottom: 20px;">Danh sách sản phẩm</h2>
	<h3 style="text-align: center;">Thêm sản phẩm</h3>
	<a href="index.php?page=product&action=insert" style="margin-left: 420px;"><img src="../template/image/insert.png" width="70px" height="60px" style="border-radius:10px;"></a>
	<table border="1" width="92%">
		<tr style="background:#7f827b; height: 50px; text-align: center; font-weight: bold;">
			<td width="13%">Tên sản phẩm</td>
			<td>Ảnh minh họa</td>
			<td width="13%">Giá</td>
			<td>Mô tả</td>
			<td width="8%">Sửa</td>
			<td width="7%">Xóa</td>
		</tr>
		<?php foreach ($_SESSION['listPage1'] as $value) { ?>
		<tr>
			<td style="font-weight: bold; text-align: center;"><?php echo $value['name'];?></td>
			<td><img src="../template/image/<?php echo $value['images'];?>" width="120px" height="100px" style="border: 1px solid #7f827b; border-radius:10px;"></td>
			<td style="text-align: center;"><?php echo number_format($value['price']);?> đồng</td>
			<td style="text-align: center;"><?php echo $value['description'];?></td>
			<td style="padding-left:5px;"><a href="index.php?page=product&action=update&id=<?php echo $value['id_Product'];?>"><img src="../template/image/edit.jpg" width="50px" height="40px" style="border-radius:10px;"></a></td>
			<td><a href="index.php?page=product&action=delete&id=<?php echo $value['id_Product'];?>"><img src="../template/image/delete.png" width="60px" height="50px"></a></td>
		</tr>
		<?php } ?>
	</table>
	<div style="padding-left: 400px; padding-top: 20px;">
	<?php 
	if (isset($_SESSION['totalPage1'])) {
		for($i = 1; $i <= $_SESSION['totalPage1']; $i++){ ?>
		<p class="page"><a href="index.php?page=product&p=<?php echo $i;?>"><?php echo $i;?></a></p>
	<?php }} ?>
</div>