<?php
include('model/news.php');
$news = new News();
$_SESSION['list_News'] = $news->getList();
$id_News = isset($_GET['id_News']) && $_GET['id_News'] != null ? $_GET['id_News'] : '';
	switch ($id_News) {
		case 1:
			if (isset($_GET['id'])) {
				$_SESSION['wedding'] = $news->getOne($_GET['id']);
				include('view/news/wedding/one.php');
			}else{
				$_SESSION['list_wedding'] = $news->getList_wedding();
				include('view/news/wedding/list.php');
			}
			break;
		case 2:
			if (isset($_GET['id'])) {
				// $_SESSION['festival'] = $news->getOne($_GET['id']);
				$festival = $news->getOne($_GET['id']);
				include('view/news/festival/one.php');
			}else{
				// $_SESSION['list_festival'] = $news->getList_festival();
				$list_festival = $news->getList_festival();
				include('view/news/festival/list.php');
			}
			break;
		case 3:
			if (isset($_GET['id'])) {
				$_SESSION['morals'] = $news->getOne($_GET['id']);
				include('view/news/morals/one.php');
			}else{
				$_SESSION['list_morals'] = $news->getList_morals();
				include('view/news/morals/list.php');
			}
			break;
		default:
			include('view/news/list.php');
			break;
	}
?>