<?php
include_once("model/user.php");
include_once("view/user/register.php");
$user = new User();
$username = $password = $fullname = "";
if (isset($_POST['btRegister'])) {
	$username = isset($_POST['txusername']) && $_POST['txusername'] != null ? $_POST['txusername'] :'';
	$password = isset($_POST['txpassword']) && $_POST['txpassword'] != null ? $_POST['txpassword'] :'';
	$fullname = isset($_POST['txfullname']) && $_POST['txfullname'] != null ? $_POST['txfullname'] :'';

	$day = isset($_POST['txday']) ? $_POST['txday'] : '';
	$month = isset($_POST['txmonth']) ? $_POST['txmonth'] :'';
	$year = isset($_POST['txyear']) ? $_POST['txyear'] :'';
	$birthday = $year.'-'.$month.'-'.$day;
	// $birthday = date("Y-m-d",strtotime($newDate));

	$sex = isset($_POST['sex']) && $_POST['sex'] == 1 ? 1 : 0;
	$user->register($username,$password,$fullname,$birthday,$sex);
	header("location:index.php?page=login");
}
?>